Write-Verbose "Get WSUS Server Object" -Verbose
$wsus = Get-WSUSServer

Write-Verbose "Connect to WSUS server configuration" -Verbose
$wsusConfig = $wsus.GetConfiguration()

Write-Verbose "Set to download updates from Microsoft Updates" -Verbose
Set-WsusServerSynchronization -SyncFromMU

Write-Verbose "Set Update Languages to English and save configuration settings" -Verbose
$wsusConfig.AllUpdateLanguagesEnabled = $false
$wsusConfig.SetEnabledUpdateLanguages("en")
$wsusConfig.Save()

Write-Verbose "Get WSUS Subscription and perform initial synchronization to get latest categories" -Verbose
$subscription = $wsus.GetSubscription()
$subscription.StartSynchronizationForCategoryOnly()

 While ($subscription.GetSynchronizationStatus() -ne 'NotProcessing') {
 Write-Host "." -NoNewline
 Start-Sleep -Seconds 5
 }

Write-Verbose "Sync is Done" -Verbose

# Write-Verbose "Disable Products" -Verbose
# Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Office" } | Set-WsusProduct -Disable
# Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Windows" } | Set-WsusProduct -Disable

Write-Verbose "Enable Products" -Verbose
# Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Windows Server 2016" } | Set-WsusProduct
Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -in (
    'Report Viewer 2008',
    'Report Viewer 2010',
    'Visual Studio 2012',
    'Visual Studio 2013',
    'Microsoft SQL Server 2011',
	'Microsoft SQL Server 2012',
	'Microsoft SQL Server 2014',
	'Microsoft SQL Server 2016',
    'Dictionary Updates for Microsoft IMEs',
    'New Dictionaries for Microsoft IMEs',
    'Office 2010',
    'Office 2013',
	'Office 2016',
	'Office 365 Client',
    'Silverlight',
	'Skype for Windows',
    'Windows 7',
    'Windows 8.1 Drivers',
    'Windows 8.1 Dynamic Update',
    'Windows 8',
	'Windows 10',
	'Windows 10 and later drivers',
	'Windows 10 and later upgrade & servicing drivers',
	'Windows 10 Anniversary Update and Later Servicing Drivers',
	'Windows 10 Anniversary Update and Later Upgrade & Servicing Drivers',
	'Windows 10 Creators Update and Later Servicing Drivers',
	'Windows 10 Creators Update and Later Upgrade & Servicing Drivers',
    'Windows Dictionary Updates',
	'Windows Server 2003',
    'Windows Server 2008 R2',
    'Windows Server 2008',
    'Windows Server 2012 R2',
    'Windows Server 2012',
	'Windows Server 2016',
    'Windows XP 64-Bit Edition Version 2003',
    'Windows XP x64 Edition',
    'Windows XP')
} | Set-WsusProduct

Write-Verbose "Disable Language Packs" -Verbose
Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Language Packs" } | Set-WsusProduct -Disable

Write-Verbose "Configure the Classifications" -Verbose

 Get-WsusClassification | Where-Object {
 $_.Classification.Title -in (
 'Critical Updates',
 'Definition Updates',
 'Feature Packs',
 'Security Updates',
 'Service Packs',
 'Update Rollups',
 'Updates')
 } | Set-WsusClassification

Write-Verbose "Configure Synchronizations" -Verbose
$subscription.SynchronizeAutomatically=$true

Write-Verbose "Set synchronization scheduled for midnight each night" -Verbose
$subscription.SynchronizeAutomaticallyTimeOfDay= (New-TimeSpan -Hours 0)
$subscription.NumberOfSynchronizationsPerDay=1
$subscription.Save()

Write-Verbose "Kick Off Synchronization" -Verbose
$subscription.StartSynchronization()

Write-Verbose "Monitor Progress of Synchronisation" -Verbose

Start-Sleep -Seconds 60 # Wait for sync to start before monitoring
 while ($subscription.GetSynchronizationProgress().ProcessedItems -ne $subscription.GetSynchronizationProgress().TotalItems) {
 $subscription.GetSynchronizationProgress().ProcessedItems * 100/($subscription.GetSynchronizationProgress().TotalItems)
 Start-Sleep -Seconds 5
 }

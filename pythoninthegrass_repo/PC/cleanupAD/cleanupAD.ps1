# hostname -- no username
#Get-ADComputer -Filter * -Property * | Select-Object Name,OperatingSystem,OperatingSystemServicePack | Export-CSV C:\Users\lstephens\Desktop\All-Windows.csv -NoTypeInformation -Encoding UTF8

#$ComputerList = get-adcomputer -filter * -Property * | Select-Object Name,OperatingSystem,OperatingSystemServicePack

# https://blogs.technet.microsoft.com/poshchap/2014/10/03/one-liner-my-take-on-finding-stale-user-and-computer-accounts/
# http://www.oxfordsbsguy.com/2014/11/20/powershell-get-adcomputer-to-retrieve-computer-last-logon-date-and-disable-them-part-2/
$DaysAgo=(Get-Date).AddDays(-90)

Get-ADComputer -Filter {(PwdLastSet -lt $DaysAgo) -or (LastLogonDate -lt $DaysAgo)} -Properties PwdLastSet,LastLogonDate,Description,OperatingSystem |

Select-Object -Property DistinguishedName,Name,Enabled,Description,OperatingSystem, `

@{Name="PwdLastSet";Expression={[datetime]::FromFileTime($_.PwdLastSet)}}, `

@{Name="LastLogonDate";Expression={[datetime]::FromFileTime($_.LastLogonDate)}} |

Export-CSV C:\Users\lstephens\Desktop\All-Windows.csv -NoTypeInformation -Encoding UTF8

## Old
#$ComputerList =  get-adcomputer -filter { OperatingSystem -notlike "*server*" } -Properties managedby | select Name,@{n="Managedby";e={($_.Managedby -split ",*..=")[1]}}
#$Output = @()
#foreach ($PC in $ComputerList) {
#
#   $PcName = $PC.Name
#   $ManagedBy = $PC.Managedby
#   if ($ManagedBy) {
#	$user = get-aduser -identity $ManagedBy -Properties Enabled,Mail,telephoneNumber
#	$UserEnabled = $user.Enabled
#	$PrimarySMTP = $user.Mail
#	$Phone = $user.telephoneNumber
#
#	$MyObject = New-Object PSObject -Property @{
#		PcName = $PC.Name
#		ManagedBy = $Managedby
#		UserEnabled = $UserEnabled
#		PrimarySMTP = $PrimarySMTP
#		Phone = $Phone
#	}
#   }else
#  {
#	$MyObject = New-Object PSObject -Property @{
#		PcName = $PC.Name
#		ManagedBy = ""
#		UserEnabled = ""
#		PrimarySMTP = ""
#		Phone = ""
#	}
#   }
#
#$Output += $MyObject
#
#}
#$Output

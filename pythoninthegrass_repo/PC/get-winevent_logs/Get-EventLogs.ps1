# Update to PS Version 5 @ https://www.microsoft.com/en-us/download/details.aspx?id=50395 && reboot
# Set-ExecutionPolicy RemoteSigned

## Windows 7+ sans computername
# Apps, Security, and System logs
Get-WinEvent -FilterHashtable @{LogName="Application","Security","System";Level=1, 2, 3;StartTime=(get-date).AddDays(-7); EndTime=(get-date).AddHours(-1)} |
Select-Object -Property Id, TimeCreated, Message, MachineName, UserId, Level, ProviderName, LogName, ContainerLog |
Export-Csv -Path $home\Downloads\$(hostname)_event_logs_$(get-date -f MMddyyyy_hhmmss).csv

# RDP logs
Get-WinEvent -FilterHashtable @{LogName="Microsoft-Windows-RemoteDesktopServices-*","Microsoft-Windows-TerminalServices-*";StartTime=(get-date).AddDays(-7); EndTime=(get-date).AddHours(-1)} |
Select-Object -Property Id, TimeCreated, Message, MachineName, UserId, Level, ProviderName, LogName, ContainerLog |
Export-Csv -Path $home\Downloads\$(hostname)_rdp_event_logs_$(get-date -f MMddyyyy_hhmmss).csv

$threshold = (Get-Date).AddDays(-90)
$computers = Get-ADComputer -Filter { OperatingSystem -like "*server*" -and OperatingSystem -like "*windows*" -and PasswordLastSet -gt $threshold } -Properties OperatingSystem,OperatingSystemServicePack,operatingSystemVersion |
	select Name, OperatingSystem, OperatingSystemServicePack,operatingSystemVersion |
	Export-Csv -Path .\windows_Servers_$(get-date -f MMddyyyy_hhmmss).csv -Append -NoTypeInformation

<#
Sources:
https://stackoverflow.com/a/22412671
https://technet.microsoft.com/en-us/library/ff730967.aspx
#>

$workingDir = (Get-Location).path
$searcher = New-Object DirectoryServices.DirectorySearcher([adsi]"")
# $searcher.filter = "(objectclass=user)"
$searcher.filter = "(&(objectCategory=User)(Name=gcaccia*))"
$users = $searcher.findall()
Out-File $workingDir\logonfile_$(get-date -f MMddyyyy_hhmmss).txt
Foreach($user in $users)
{
  if($user.properties.item("lastLogon") -ne 0)
  {
    $a = [datetime]::FromFileTime([int64]::Parse($user.properties.item("lastLogon")))
    "$($user.properties.item(`"name`")),$a" >> $workingDir\logonfile_$(get-date -f MMddyyyy_hhmmss).csv -NoTypeInformation
  }
}
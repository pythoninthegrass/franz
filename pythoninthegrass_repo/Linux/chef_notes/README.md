# bootstrap_chef-server
Install Chef Server on CentOS 7

## CREDIT ##
* linuxacademy.com
* https://docs.chef.io/install_server_pre.html
* https://github.com/hyeomans/chef-lab/blob/master/provision/bootstrap-chef-server.sh
* https://stackoverflow.com/questions/13322485/how-to-get-the-primary-ip-address-of-the-local-machine-on-linux-and-os-x/25851186#25851186
* stripping single quotes via array and regex sub @ https://unix.stackexchange.com/a/194236
* https://unix.stackexchange.com/questions/400038/send-stderr-to-stdout-for-purposes-of-grep
* https://unix.stackexchange.com/questions/13466/can-grep-output-only-specified-groupings-that-match
* https://superuser.com/questions/384963/yum-hangs-and-wont-respond
* http://ask.xmodulo.com/list-all-installed-packages-centos-rhel.html
* https://unix.stackexchange.com/questions/138202/can-i-chain-pgrep-with-kill/138206#138206
* https://stackoverflow.com/questions/9349616/bash-eof-in-if-statement
* https://stackoverflow.com/questions/16362402/save-file-to-specific-folder-with-curl-command/16363115#16363115
* https://stackoverflow.com/questions/40429865/how-to-exit-bash-function-if-error/46597508#46597508

## Notes ##
-

#!/usr/bin/env bash

# logging
logTime=$(date +%Y-%m-%d:%H:%M:%S)
bootstrapLog="/tmp/$(basename "$0" | cut -d. -f1)_$logTime.log"
exec &> >(tee -a "$bootstrapLog")

# current user
loggedInUser=$(who | awk 'NR==1{print $1}')
scriptDir=$(cd "$(dirname "$0")" && pwd)
#echo $scriptDir

# ensure running as root
if [[ "$(id -u)" != "0" ]]; then
  exec sudo "$0" "$@"
fi

# set $IFS to eliminate whitespace in pathnames
IFS="$(printf '\n\t')"

# env vars
# chef_email shouldn't have quotes, secure_pass does
chef_user='lstephens'
declare -a chef_name_array=(Lance Stephens)
chef_full_name=("${chef_name_array[@]//\'/}")
secure_pass='' # fill this out locally because security; might need single quotes to expand with retention
chef_email='lstephens@napster.com'
fqdn='chef-server.corp.rhapsody.com'
ip_addr="$(ip route get 1 | awk '{print $NF;exit}')"

# remove desktop icons
gnome_de=$(pgrep -a 'gnome-session' | awk '{print $2}')
if [[ "$gnome_de" = '/usr/libexec/gnome-session-binary' ]]; then
    gsettings set org.gnome.nautilus.desktop trash-icon-visible false
    gsettings set org.gnome.nautilus.desktop home-icon-visible false
    gsettings set org.gnome.nautilus.desktop volumes-visible false
fi

# directory permissions based on current user
if [[ ! -d "/home/$loggedInUser/certs" ]]; then
    mkdir -p "/home/$loggedInUser/certs"
    chown -R "$loggedInUser":"$loggedInUser" "/home/$loggedInUser"
fi

# kill previous yum operations
if [[ $(pgrep yum) != '' ]]; then
    pgrep yum | xargs sudo kill -9
    rm -f /var/run/yum.pid
    rm -rf /var/tmp/*
    rm -rf /var/cache/yum/*
    rpm --rebuilddb
    yum clean --noplugins all
fi

# epel
# TODO: more robust way to check if epel is installed -- false negative if nothing's been installed from epel
[[ $(yum list installed | grep @epel | awk '{print $3}') = '' ]] && yum install --noplugins -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
[[ ! -f '/etc/yum.repos.d/mcepl-vim8-epel-7.repo' ]] && curl -L https://copr.fedorainfracloud.org/coprs/mcepl/vim8/repo/epel-7/mcepl-vim8-epel-7.repo -o /etc/yum.repos.d/mcepl-vim8-epel-7.repo
yum update --noplugins -y

# install ultimate vim and update
vim_install() (
    yum install --noplugins -y vim
    if [[ ! -d "/home/$loggedInUser/.vim_runtime" ]]; then
        git clone --depth=1 https://github.com/amix/vimrc.git "/home/$loggedInUser/.vim_runtime"
        su "$loggedInUser" -c "sh /home/$loggedInUser/.vim_runtime/install_awesome_vimrc.sh"
    elif [[ -d "/home/$loggedInUser/.vim_runtime" ]]; then
        cd "/home/$loggedInUser/.vim_runtime" || exit
        git pull --rebase
        su "$loggedInUser" -c "sh /home/$loggedInUser/.vim_runtime/install_awesome_vimrc.sh"
    fi
)
# subshell with `-e` flag will exit from current subshell on any error inside function
(
  set -e
  vim_install
)
# check subshell error code
if [[ "$?" -eq 0 ]]; then
    yum update --noplugins vim*
fi

# update then install tools + chef-server packages
# TODO: add `> /dev/null` after wget once finished testing (suppress download progress)
yum groupinstall -y "Development tools" --setopt=group_package_types=mandatory,default

if [[ ! -f '/bin/chef-server-ctl' ]] && [[ ! -f '/tmp/chef-server-core-12.17.33-1.el7.x86_64.rpm' ]]; then
    cd /tmp && { curl -LJO https://packages.chef.io/files/stable/chef-server/12.17.33/el/7/chef-server-core-12.17.33-1.el7.x86_64.rpm ; cd -; }
    rpm -Uvh /tmp/chef-server-core-12.*.x86_64.rpm || exit
    cd "$scriptDir" || exit
elif [[ ! -f '/bin/chef-server-ctl' ]] && [[ -f '/tmp/chef-server-core-12.*.x86_64.rpm' ]]; then
    rm /tmp/chef-server-core-12.*.x86_64.rpm
    cd /tmp && { curl -LJO https://packages.chef.io/files/stable/chef-server/12.17.33/el/7/chef-server-core-12.17.33-1.el7.x86_64.rpm ; cd -; }
    rpm -Uvh /tmp/chef-server-core-12.*.x86_64.rpm || exit
    cd "$scriptDir" || exit
fi

# SELinux whitelist
if [[ $(getenforce) != 'Permissive' ]]; then
    setenforce Permissive
fi

# FirewallD
if [[ $(firewall-cmd --zone public --list-services | grep -oE 'http.*https') != 'http https' ]]; then
    firewall-cmd --permanent --zone public --add-service http
    firewall-cmd --permanent --zone public --add-service https
    firewall-cmd --reload
fi

# configure hosts file for our internal network
hostname "chef-server.corp.rhapsody.com"

echo "chef-server.corp.rhapsody.com" | tee /etc/hostname

if [[ $(grep 'ALL: ::1' /etc/hosts.allow | awk '{print $0}') != 'ALL: ::1' ]]; then
    echo 'ALL: ::1' >> /etc/hosts.allow
fi

# clobber /etc/hosts w/CentOS 7 defaults + dynamic IP and chef-server FQDN
if [[ $(grep chef-server /etc/hosts | awk '{print $2}') != "$fqdn" ]]; then
cat <<- EOF > /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
$ip_addr    $fqdn
EOF
fi

# chef-server first-run install
chef-server-ctl reconfigure
# printf "\033c" # clever way to clear screen via ASCII
chef-server-ctl user-create "$chef_user" "${chef_full_name[@]}" "$chef_email" "$secure_pass" --filename "/home/$loggedInUser/certs/$chef_user-user-rsa"

chef-server-ctl org-create corp "$fqdn" --association_user "$chef_user" --filename "/home/$loggedInUser/certs/$fqdn-validator.pem"

chef-server-ctl install chef-manage
chef-server-ctl reconfigure --accept-license
chef-manage-ctl reconfigure --accept-license

chef-server-ctl install opscode-reporting
chef-server-ctl reconfigure
opscode-reporting-ctl reconfigure --accept-license

# success!
# printf "\033c"
echo "Chef Console is ready: http://$fqdn with login: $chef_user password: $secure_pass"

unset IFS

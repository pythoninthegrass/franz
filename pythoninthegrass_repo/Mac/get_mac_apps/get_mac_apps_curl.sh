#!/bin/bash

# From http://www.getmacapps.com/
# Usage:
# curl -s http://www.getmacapps.com/raw/tkabftzs269 | sh

mkdir ~/getmacapps_temp
cd ~/getmacapps_temp

# Installing Chrome
curl -L -O "https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg"
hdiutil mount -nobrowse googlechrome.dmg
cp -R "/Volumes/Google Chrome/Google Chrome.app" /Applications
hdiutil unmount "/Volumes/Google Chrome"
rm googlechrome.dmg

# Installing Google Drive
curl -L -O "https://dl-ssl.google.com/drive/installgoogledrive.dmg"
hdiutil mount -nobrowse installgoogledrive.dmg
cp -R "/Volumes/Install Google Drive/Google Drive.app" /Applications
hdiutil unmount "/Volumes/Install Google Drive"
rm installgoogledrive.dmg

# Installing caffeine
curl -L -o Caffeine.zip "http://download.lightheadsw.com/download.php?software=caffeine"
unzip Caffeine.zip
mv Caffeine.app /Applications
rm Caffeine.zip

# Installing iterm2
curl -L -o iTerm2.zip "https://iterm2.com/downloads/stable/iTerm2-2_1_4.zip"
unzip iTerm2.zip
mv iTerm.app /Applications
rm iTerm2.zip

# Installing GitHub
curl -L -o mac_GitHub.zip "https://central.github.com/mac/latest"
unzip mac_GitHub.zip
mv GitHub.app /Applications
rm mac_GitHub.zip

# Installing Tower
curl -L -o tower.zip "http://www.git-tower.com/download"
unzip tower.zip
mv "Tower.app" /Applications
rm tower.zip

# Installing Alfred
curl -L -o Alfred.zip "https://cachefly.alfredapp.com/Alfred_2.8.2_432.zip"
unzip Alfred.zip
mv "Alfred 2.app" /Applications
rm Alfred.zip

# Installing Notational Velocity
curl -L -O "http://notational.net/NotationalVelocity.zip"
unzip NotationalVelocity.zip
mv "Notational Velocity.app" /Applications
rm NotationalVelocity.zip

# Installing The Unarchiver
curl -L -o TheUnarchiver.dmg "http://unarchiver.c3.cx/downloads/TheUnarchiver3.10.1.dmg"
hdiutil mount -nobrowse TheUnarchiver.dmg
cp -R "/Volumes/The Unarchiver/The Unarchiver.app" /Applications
rm TheUnarchiver.dmg

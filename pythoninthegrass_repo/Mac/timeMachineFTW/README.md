# archive_mac
Kick off Time Machine backup to OS X Server share with exclusions.

## CREDIT ##
* Logging @ http://unix.stackexchange.com/a/145654
* Speed Up Time Machine Backup via `sysctl` @ http://lifehacker.com/temporarily-speed-up-a-time-machine-backup-with-a-termi-1785213919
* Mass Deploying Time Machine via `tmutil` @ http://krypted.com/mac-security/mass-deploying-time-machine/
* `wc` syntax @ http://stackoverflow.com/q/10238363
* Directory arrays @ http://stackoverflow.com/questions/21668471/bash-script-create-array-of-all-files-in-a-directory

## Notes ##
* `find`
    * "find ." returns './file'
    * "find *" strips filepath
* `tmutil`
    * Needs _b (--block)_ switch to wait for backup to finish

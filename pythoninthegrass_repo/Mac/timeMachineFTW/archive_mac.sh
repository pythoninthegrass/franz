#!/usr/bin/env bash


# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d:%H:%M:%S)
archiveLog="/tmp/archive_mac_$logTime.log"
exec &> >(tee -a "$archiveLog")

# Current user
# loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)

# Working directory
# scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
if [ "$(whoami)" != "root" ]; then
    echo "Sorry, you need super user privileges to run this script."
    exit 1
fi

# Verify if backups have occurred. awk needed to get rid of empty file name
tmStatus=$(/usr/bin/tmutil listbackups | wc -l | awk '{print $1}')
if [ "$tmStatus" -lt "1" ]; then
    return
else
    echo "This Mac has already been backed up. Exiting now."
    exit 1
fi

# Enable Time Machine
sudo /usr/bin/tmutil enable

# Set $IFS to eliminate whitespace in pathnames
IFS="$(printf '\n\t')"

# Exclusions for loop
exclusionsArray=($(find /Users -maxdepth 2 -type d | grep -E 'Dropbox|Google Drive'))
# echo "${exclusionsArray[@]}"
for d in "${exclusionsArray[@]}"; do
    yes | /usr/bin/tmutil addexclusion $d
    /usr/bin/tmutil isexcluded "$d"
done

# Fixed-path exclusion
/usr/bin/tmutil addexclusion -p '/System'

unset IFS

# Disable CPU throttling during backups
sysctl debug.lowpri_throttle_enabled=0

# Backup location OS X Server
if [ ! -e "/Volumes/Time Machine Backups/Backups.backupdb/" ]; then
    /usr/bin/tmutil setdestination -p afp://rhapadmin@its-a-me-mini/Backups
fi

# Start/stop Time Machine backup w/block switch to wait until finished. Prevent display, system, and disk from sleeping until job completes.
/usr/bin/caffeinate -dim /usr/bin/tmutil startbackup -b

# Postpone unsetting CPU throttling while backup is running
# inProgress=$(/usr/bin/tmutil status | grep 'Running' | grep -o '[1]')
# echo $inProgress
# Running is 1 for backing up; 0 for finished (i.e., off)
# while [ "$inProgress" -eq "1" ]; do
#     sleep 5 # wait
# done

# Unset CPU throttling after successful run
sysctl debug.lowpri_throttle_enabled=1

# Disable Time Machine
sudo /usr/bin/tmutil disable

exit 0

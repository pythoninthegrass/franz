#!/usr/bin/env bash


# Exit upon failed command
# set -e

# Install log
logTime=$(date +%Y-%m-%d:%H:%M:%S)
installLog="/tmp/$(basename "$0" | cut -d. -f1)_$logTime.log"
exec &> >(tee -a "$installLog")

# Current user
loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)
scriptDir=$(cd "$(dirname "$0")" && pwd)
#echo $scriptDir

# Check for root privileges
if [ "$(whoami)" != "root" ]; then
  echo "Sorry, you need super user privileges to run this script."
  exit 1
fi

# Remove existing printers
echo "Current printers: "
# lpstat -a | cut -d " " -f1 # Show all printers
printerArray=($(lpstat -a | cut -d " " -f1 | grep -E 'HP|WorkCentre|Xerox'))
echo "${printerArray[@]}"
read -r -p "Do you want to remove ALL printers? [Y/n]" response
response=${response,,} # tolower
    if [[ $response =~ ^(yes|y| ) ]]; then
        for p in "${printerArray[@]}"; do
            yes | lpadmin -x $p
            echo "$p was removed successfully."
        done
    else
        echo "No printers have been removed."
    fi

# Fix absolute path to SMB share PPDs dir
ppd="/Volumes/IT Apps/Mac OS X Printers/Automated Mac Printers/PPDs/"
printDrivers=$(echo *.gz*)

# Mount file share with PPDs
if [[ ! -e "/Volumes/IT Apps/" ]]; then
    osascript -e 'mount volume "smb://file-corp-1202/IT Apps/Mac OS X Printers/Automated Mac Printers/PPDs"'
    echo "File share successfully mounted."
else
    echo "File share is already mounted."
fi

# Install PPDs
cd $ppd || exit
# echo $printDrivers
cp $printDrivers /Library/Printers/PPDs/Contents/Resources/

# Map HP_Color_LaserJet_4700dn
lpadmin -p HP_Color_LaserJet_4700dn -L "Lake Sammamish" -o printer-is-shared="False" -E -v 'lpd://'10.151.58.12'/' -P '/Library/Printers/PPDs/Contents/Resources/HP Color LaserJet 4700.gz'

# Map HP_OfficeJet_Pro_8620
# lpadmin -p HP_OfficeJet_Pro_8620 -L "Ruby & Chelsea" -o printer-is-shared="False" -E -v 'lpd://'10.151.58.68'/' -P '/Library/Printers/PPDs/Contents/Resources/HP Officejet Pro 8620.ppd.gz'

# Map Xerox_WorkCentre_7335
# Xerox Print Driver 3.68.0.pkg >> XeroxPrintDriver_3.121.0_1854.dmg as of 4/3/17
lpadmin -p Xerox_WorkCentre_7335 -L "Puget Sound" -o printer-is-shared="False" -E -v 'lpd://'10.151.58.10'/' -P '/Library/Printers/PPDs/Contents/Resources/Xerox WorkCentre 7335.gz'

# Map Xerox_WorkCentre_3550
# Xerox_WorkCentre_3550_CD-Driver_1.10.dmg
lpadmin -p Xerox_WorkCentre_3550 -L "Lake Washington" -o printer-is-shared="False" -E -v 'lpd://'10.151.58.11'/' -P '/Library/Printers/PPDs/Contents/Resources/Xerox WorkCentre 3550.gz'

echo "Successfully mapped all printers."

exit 0

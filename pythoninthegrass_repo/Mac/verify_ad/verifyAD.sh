#!/usr/bin/env bash


# Source: https://github.com/hunty1/strapper

# hard coded DET variables
domain="corp.rhapsody.com"         # Our AD domain name
sbase="DC=corp,DC=rhapsody,DC=com"    # Our default search base when searching for user accounts etc
sitebase="CN=Domain Users,CN=Users,DC=corp,DC=rhapsody,DC=com"   # Our search base when looking for Site information only

echo "In order to continue, please login with your AD username and password"

# Function to check the user credentials - Loop through if invalid credentials provided.
checkusercreds()
{
# user entered variables
# Accept user input while masking what is entered @ http://stackoverflow.com/a/1923893
echo "Enter Your Username:"
read uname
unset password
prompt="Enter Password:"
while IFS= read -p "$prompt" -r -s -n 1 char
do
    if [[ $char == $'\0' ]]
    then
        break
    fi
    prompt='*'
    password+="$char"
done
/usr/bin/ldapsearch -LLL -H ldap://$domain -x -D $uname@$domain -w $password -b $sbase sAMAccountName=$uname > /dev/null
# echo $?
}
checkusercreds
while [ $? -ne "0" ]; do
    echo "Invalid username or password combination! Please try again."
    checkusercreds
done
echo " Authentication Successful! "

#!/usr/bin/env bash


# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d:%H:%M:%S)
resetLog="/tmp/pkg_install_$logTime.log"
exec &> >(tee -a "$resetLog")

# Current user
loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)

# Working directory
scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
if [ "$(whoami)" != "root" ]; then
    echo "Sorry, you need super user privileges to run this script."
    exit 1
fi

# Set $IFS to eliminate whitespace in pathnames
IFS="$(printf '\n\t')"

# PLISTs
declare -a plistsArray=(
    /Users/$loggedInUser/Library/Preferences/MobileMeAccounts.plist
    /Users/$loggedInUser/Library/Caches/com.apple.iCloudHelper
    /Users/$loggedInUser/Library/Library/Accounts/Accounts3.sqlite-wal
    /Users/$loggedInUser/Library/Library/Caches/CloudKit/CloudKitMetadata-wal
    /Users/$loggedInUser/Library/Library/Caches/com.apple.imfoundation.IMRemoteURLConnectionAgent/Cache.db-shm
    /Users/$loggedInUser/Library/Library/Caches/com.apple.imfoundation.IMRemoteURLConnectionAgent/Cache.db-wal
    /Users/$loggedInUser/Library/Library/Caches/com.apple.nsservicescache.plist
    /Users/$loggedInUser/Library/Library/Cookies/com.apple.appstore.cookies
    /Users/$loggedInUser/Library/Library/Caches/CloudKit/com.apple.accountsd/
    /Users/$loggedInUser/Library/Library/Containers/com.apple.internetaccounts/
    /Users/$loggedInUser/Library/Library/Preferences/MobileMeAccounts.plist
    /Users/$loggedInUser/Library/Library/Preferences/com.apple.accountsd.plist
    /Users/$loggedInUser/Library/Library/Preferences/com.apple.ids.service.com.apple.private.alloy.screensharing.plist
    /Users/$loggedInUser/Library/Library/Preferences/com.apple.internetaccounts.plist
    /Users/$loggedInUser/Library/Library/SyncedPreferences/icbaccountsd.plist
)
# echo $plistsArray

# Remove PLISTs
for f in "${plistsArray[@]}"; do
    echo "Removed $f."
    [ -f "$f" ] && rm -f "$f"
done

# Stop preferences daemon to load new PLISTs
killall cfprefsd

# Reset NVRAM
if [ $? -ne 0 ]; then
    sudo /usr/sbin/nvram -c
    echo "Reset NVRAM successfully."
fi

unset IFS

# Reboot
if [ $? -ne 0 ]; then
    echo "Restarting now. Hit CTRL-C to cancel."
    sleep 5s
    sudo reboot
else
    echo "No changes were made. Reboot cancelled."
    exit
fi

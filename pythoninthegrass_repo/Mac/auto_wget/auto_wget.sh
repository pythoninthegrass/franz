#!/usr/bin/env bash


# Source:
# Usage: $ run 5 URL

# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d:%H:%M:%S)
downloadLog="/tmp/wget_$logTime.log"
exec &> >(tee -a "$downloadLog")

# Current user
loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)

# Working directory
scriptDir=$(cd "$(dirname "$0")" && pwd)

# wget function with user input
function auto_wget() {
    number=$1
    shift
    for n in $(seq $number); do
        # $@ == URL; need to pass end of URL/filename behind appended number
        wget $@ -O ${n}_mobile-1885-RhapsodyApp-debug.apk && sleep 5
    done
}

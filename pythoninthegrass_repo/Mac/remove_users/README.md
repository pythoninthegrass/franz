# remove_users
Delete all users via `dscl` with exclusions.

## CREDIT ##
* `dscl` and bulk of script @ https://www.jamf.com/jamf-nation/discussions/19031/remove-users-nameofuser-but-user-is-still-listed-in-user-pane-of-system-preferences#responseChild113597
* `basename` string slicing with script variable name `$0` @ http://stackoverflow.com/a/26753382

## NOTES ##
* Refashioned JAMF script into an array with IFS correction to account for "Deleted Users" volume from previously deleted accounts via Users & Groups GUI.

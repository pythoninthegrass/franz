#!/usr/bin/env bash


# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d:%H:%M:%S)
removalLog="/tmp/$(basename "$0" | cut -d. -f1)_$logTime.log"
exec &> >(tee -a "$removalLog")

# Current user
# loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)

# Working directory
# scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
if [[ "$(whoami)" != "root" ]]; then
    echo "Sorry, you need super user privileges to run this script."
    exit 1
fi

# Set $IFS to eliminate whitespace in pathnames
IFS="$(printf '\n\t')"

# User array excluding local admin and McAfee service account
userList=($(dscl . list /Users UniqueID | awk '$2 > 500 && $2 < 999 {print $1}' | awk '!/rhapadmin/ && !/mfe/'))

# Remove users other than local admin and McAfee service account. Clean up home directory if necessary
for item in "${userList[@]}"; do
    #echo $item
    sysadminctl -deleteUser $item
    if [[ -d "/Users/$item" ]]; then
		/usr/bin/sudo rm -rf "/Users/$item"
	else
        echo "$item home directory was previously removed by sysadminctl"
    fi
done

unset IFS

exit 0

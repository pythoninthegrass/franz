# reset_bluetooth
Tired of deleting wifi PLISTs by hand.

##CREDIT:##
* env tightening @ http://superuser.com/a/1133205
* PLISTs location @ http://osxdaily.com/2016/09/22/fix-wi-fi-problems-macos-sierra/
* Error logging @ http://unix.stackexchange.com/a/145654
* Ben Daschel @ https://github.com/supercoffee fixed my for loop modified from https://www.cyberciti.biz/faq/use-a-for-loop-to-remove-file-in-unix/ and http://stackoverflow.com/a/20203051. He also added the "declare" bit for the plistsArray.

##Notes##

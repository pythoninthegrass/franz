# ping
for n in {1..5}; do ping -c 1 8.8.4.4 && sleep 2; done

# wget
for n in {1..3}; do wget http://distro.ibiblio.org/damnsmall/current/dsl-4.4.10-initrd.iso -O ${n}_dsl-4.4.10-initrd.iso && sleep 60; done

# brew link
for n in cvs ffmpeg lame libvo-aacenc x264 xvid; do brew link $n; done

# QA APK build
for n in {1..3}; do wget http://10.151.59.242:8080/job/ContinuumDevelopBuild/1885/artifact/rhapsody-android-core/Continuum/mobile/build/outputs/apk/mobile-1885-RhapsodyApp-debug.apk -O ${n}_mobile-1885-RhapsodyApp-debug.apk && sleep 5; done

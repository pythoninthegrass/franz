#!/usr/bin/env bash


# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d:%H:%M:%S)
archiveLog="/tmp/archive_mac_$logTime.log"
exec &> >(tee -a "$archiveLog")

# Current user
loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)

# Working directory
scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
if [ $(whoami) != "root" ]; then
    echo "Sorry, you need super user privileges to run this script."
    exit 1
fi

# Rename Mac based on serial number
SN=$(system_profiler | grep "Serial Number (system)" | awk '{print $4}')
scutil --set LocalHostName NPSEA-$SN
scutil --set HostName NPSEA-$SN
scutil --set ComputerName NPSEA-$SN

exit 0

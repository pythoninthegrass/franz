#!/usr/bin/env bash


# SOURCES:
# /u/Nikolas Britton @ https://www.systutorials.com/5450/improving-sshscp-performance-by-choosing-ciphers/
# /u/Albert @ http://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux/17072017#17072017

# Exit upon failed command
# set -e

# Install log
logTime=$(date +%Y-%m-%d:%H:%M:%S)
cipherLog="/tmp/pkg_install_$logTime.log"
exec &> >(tee -a "$cipherLog")

# Current user based on OS
if [ "$(uname)" == "Darwin" ]; then
    loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    loggedInUser=$(whoami)
fi

# Working directory
scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
# if [ $(whoami) != "root" ]; then
#     echo "Sorry, you need super user privileges to run this script."
#     exit 1
# fi

# Set $IFS to eliminate whitespace in pathnames
IFS="$(printf '\n\t')"

# ciphers
declare -a cipherArray=(
    3des-cbc
    aes128-cbc
    aes128-ctr
    aes128-gcm@openssh.com
    aes192-cbc
    aes192-ctr
    aes256-cbc
    aes256-ctr
    aes256-gcm@openssh.com
    arcfour
    arcfour128
    arcfour256
    blowfish-cbc
    cast128-cbc
    chacha20-poly1305@openssh.com
    rijndael-cbc@lysator.liu.se
)
# echo $cipherArray

# Test throughput
for a in "${cipherArray[@]}"; do
    dd if=/dev/zero bs=1000000 count=1000 2> /dev/null | ssh -c $i localhost "(time -p cat) > /dev/null" 2>&1 | grep real | awk '{ print "'$i': "1000 / $2" MB/s" }'
done

unset IFS

exit 0

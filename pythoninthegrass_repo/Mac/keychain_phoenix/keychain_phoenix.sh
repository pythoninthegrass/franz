#!/usr/bin/env bash

# Idea inspired by Addison Martel on Thu Jul 7, 2016
# Run once per day in JSS Self Service. # danger zone

# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d_%I:%M:%S%p)
resetLog="/tmp/$(basename "$0" | cut -d. -f1)_$logTime.log"
exec &> >(tee -a "$resetLog")

# Current user
loggedInUser=$(stat -f%Su /dev/console)

# Working directory
# scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
if [[ $(whoami) != "root" ]]; then
    echo "Sorry, you need super user privileges to run this script."
    exit 1
fi

# Move existing keychains
cd /Users/$loggedInUser/Library/Keychains/ || exit
if [[ ! -e "/Users/$loggedInUser/Library/Keychains/Old" ]]; then
    mkdir -p /Users/$loggedInUser/Library/Keychains/Old
    echo "Created backup keychain directory successfully."
fi

# Enable glob pattern matching, then move all but the Old keychain directory
shopt -s extglob
mkdir -p "/Users/$loggedInUser/Library/Keychains/Old/Old_Keychain_$logTime" && mv !(Old) "$_"
if [[ $? = 0 ]]; then echo "Old keychain was moved successfully."
else
    echo "Resetting keychain failed. Try, try again"
    shopt -u extglob
    exit 1
fi
shopt -u extglob

# Prompt for reboot
confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        *)
            false
            ;;
    esac
}
confirm "Do you want to reboot now to reset the keychain? [Y/n]?" && /usr/bin/sudo sh -c "reboot"

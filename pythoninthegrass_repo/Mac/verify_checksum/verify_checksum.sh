#!/usr/bin/env bash


# Source: /u/jabaldonedo @ http://stackoverflow.com/a/12900580
# sed spacing: /u/Marplesoft @ http://stackoverflow.com/a/29779745
# Usage: script.sh file1.txt file2.txt

# ToDo: accept more than two files, store files in array, then parse in for loop
# e.g., for FILE1 in "$@"; do wc $FILE1; done
# See: https://www.lifewire.com/pass-arguments-to-bash-script-2200571


# Exit upon failed command
# set -e

# Logs
logTime=$(date +%Y-%m-%d:%H:%M:%S)
archiveLog="/tmp/md5_$logTime.log"
exec &> >(tee -a "$archiveLog")

# Current user
loggedInUser=$(ls -l /dev/console | cut -d " " -f 4)

# Working directory
scriptDir=$(cd "$(dirname "$0")" && pwd)

# Check for root privileges
if [ "$(whoami)" != "root" ]; then
    echo "Sorry, you need super user privileges to run this script."
    exit 1
fi

# pretty sed indent function w/4 spaces
indent() { sed 's/^/    /'; }

# Set $IFS to eliminate whitespace in pathnames
IFS="$(printf '\n\t')"

# Files to compare
file1=$(md5 $1 | awk '{print $6}')
file2=$(md5 $2 | awk '{print $6}')

# Verify MD5 checksum
if [ "$file1" = "$file2" ]; then
    echo "Files are identical. MD5 is $file1"
else
    echo -e "Files are NOT the same: \n"$1": "$file1""
    echo "VERSUS" | indent
    echo -e ""$2": "$file2""
fi

unset IFS

exit 0
